# gqlgen + twirp microservice

soon...

## Build
```
$ dep ensure
$ docker-compose up -d --build
```

## Run
```
$ cd gateway/ && go run main.go
$ cd twix/ && go run cmd/server/main.go
```
Open <http://localhost:8081/playground> in your browser.  

## example query
```
{
  candy(id:1){
    id
    name
  }
}
```


## ToDo
- [ ] postgresql connection
- [ ] redis connection
- [ ] rabbitmq connection
- [ ] elasticsearch connection
- [ ] makefile builder
- [ ] cron
- [ ] file server
- [ ] unit & fuzzing testing impl