package main

import (
	"log"
	"net/http"
	"os"

	"github.com/99designs/gqlgen/handler"
	"github.com/joho/godotenv"
	"gitlab.com/qwzar/hcomb/gateway/graph"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}

	twixURL := os.Getenv("TWIX_SERVICE_URL")

	s := graph.New(twixURL)
	if err != nil {
		log.Fatal(err)
	}

	http.Handle("/graphql", handler.GraphQL(graph.NewExecutableSchema(s)))
	http.Handle("/playground", handler.Playground("Gateway", "/graphql"))

	log.Fatal(http.ListenAndServe(":8081", nil))
}
