package twix

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/qwzar/hcomb/twix/rpc/twix"
)

type Client struct {
	service twix.TwixService
}

//NewClient init
func NewClient(url string) (*Client, error) {
	c := twix.NewTwixServiceProtobufClient(url, &http.Client{})
	fmt.Println(url)

	return &Client{c}, nil
}

//GetCandy resolver
func (c *Client) GetCandy(ctx context.Context, id int32) (*twix.Candy, error) {

	var (
		r   *twix.GetCandyResponse
		err error
	)

	r, err = c.service.GetCandy(context.Background(), &twix.GetCandyRequest{Id: id})

	if err != nil {
		return nil, err
	}

	return &twix.Candy{
		Id:   r.Candy.Id,
		Name: r.Candy.Name,
	}, nil
}
